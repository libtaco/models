MODELS
======

Models contains robot descriptions. Only KUKA iiwa currently contains the necessary sdf and urdf for use with [taco]. Below are links to original source of the files. Please review the original source for licenses and other relevant information.

* [Barrett WAM]
* [Kinova JACO]
* [KUKA iiwa]
* [KUKA LWR]
* [PR2]
* PUMA
* [UR10]

[Barrett WAM]:https://github.com/jhu-lcsr/barrett_model
[Kinova JACO]:https://github.com/Kinovarobotics/kinova-ros/tree/master/kinova_description
[KUKA iiwa]:https://github.com/kuka-isir/iiwa_description
[KUKA LWR]:https://github.com/CentroEPiaggio/kuka-lwr/tree/master/lwr_description
[PR2]:https://github.com/PR2/pr2_common/tree/indigo-devel/pr2_description
[UR10]:https://github.com/ros-industrial/robot_movement_interface/tree/master/dependencies/ur_description

URDF vs SDF
-----------

URDF

* ROS robot format
* intuitive: link pose relative to parent joint
* compact format easier to read
* only supports kinematic trees

SDF

* gazebo file format
* supports closed kinematic chains
* nonintuitive: link pose relative to model origin
* better documented
* contains gazebo plugins
* also supports closed-kinematic chains

Generating the URDF and SDF
---------------------------

By default, the urdf is already generated for the kuka iiwa14. If you need to use other robots, then here are the step required for the [taco] dynamics engine to correctly parse the URDF, and for Gazebo to correctly parse the [taco] controller plugins.

#### Generate the URDF

1. Generate the urdf

        rosrun xacro xacro.py -o model.urdf model.urdf.xacro

2. rename the link that is fixed to the world frame as "base_link"
3. comment out the inertia for the "base_link". e.g.

        <!--inertial>
           <origin xyz="0 0 0.14" rpy="0 0 0"/>
           <mass value="4.0"/>
           <inertia ixx="0.4" iyy="0.4" izz="0.4" ixy="0.0" iyz="0.0" ixz="0.0"/>
        </inertial-->

4. remove all fixed joints, and merge all links that are fixed together
5. check that the mesh paths are correct. e.g.

        <mesh filename="package://iiwa/meshes/iiwa14/visual/link_3.stl"/>

#### Generate the SDF

The sdf can be generated from the urdf using the below process. See [urdf in gazebo].

1. uncomment the inertia for the base link. e.g.

        <inertial>
           <origin xyz="0 0 0.14" rpy="0 0 0"/>
           <mass value="4.0"/>
           <inertia ixx="0.4" iyy="0.4" izz="0.4" ixy="0.0" iyz="0.0" ixz="0.0"/>
        </inertial>

2. convert the urdf to sdf using gazebo

        gz sdf -p model.urdf > model.sdf

3. comment out the inertia for the base link. (i.e. undo step 1)

4. Add a pose of 0 to all links

        <joint name='j6' type='revolute'>
          <pose>0 0 0 0 0 0</pose>

5. Add grounding connection, by adding these links and joints to the sdf model.

        <joint name='fixed' type='fixed'>
          <child>base_link</child>
          <parent>world</parent>
        </joint>

6. Add in desired plugins

        <sensor name="end_effector" type="contact">
          <contact>
           <collision>l6-c</collision>
          </contact>
          <plugin name="force_sensor_plugin" filename="libForceSensorPlugin.so" />
        </sensor>


#### Convert SDF to URDF

SDF supports closed kinematic chains. Additionally, we need to specify our Gazebo plugins in the SDF. Therefore, it is sometimes convenient to maintain the SDF, and generate to the URDF. To convert the SDF to a URDF, use the models/sdf2urdf converter.


Visualize the model
-------------------

#### SDF

Use the models/update_gazebo_models.sh script to create links to the models in the ~/.gazebo/models/ directory. Then open Gazebo, insert the model, and play around.

#### URDF

Check the urdf format and visualize:
     
     check_urdf iiwa14.urdf
     roslaunch urdf_tutorial display.launch model:=iiwa14.urdf gui:=true

The visualization requires rviz. Use the below commands to install rviz.

     sudo apt-get install ros-kinetic-rviz
     sudo apt-get install ros-kinetic-joint-state-publisher
     rosdep install rviz joint_state_publisher
     cd ~/catkin_ws
     rosmake rviz 
     rosmake joint_state_publisher






[urdf in gazebo]:http://gazebosim.org/tutorials/?tut=ros_urdf
[taco]:libtaco.bitbucket.io
