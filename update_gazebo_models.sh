echo "Creating links from [taco/models/] to [~/.gazebo/models/]"

echo "linking $(pwd)/iiwa to ~/.gazebo/models/"
ln -sf $(pwd)/iiwa ~/.gazebo/models/

echo "linking $(pwd)/iiwa_no_collision to ~/.gazebo/models/"
ln -sf $(pwd)/iiwa_no_collision ~/.gazebo/models/
ln -sf $(pwd)/iiwa/launch $(pwd)/iiwa_no_collision/
ln -sf $(pwd)/iiwa/meshes $(pwd)/iiwa_no_collision/
ln -sf $(pwd)/iiwa/urdf $(pwd)/iiwa_no_collision/
echo $(pwd)

echo "linking $(pwd)/quadruped to ~/.gazebo/models/"
ln -sf $(pwd)/quadruped ~/.gazebo/models/

# create link to all models
#for D in ./*/
#do 
#  echo "$(pwd)/${D}"   # your processing here
#  ln -sf $(pwd)/$D ~/.gazebo/models/
#done



