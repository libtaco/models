SDF to URDF Conversion
======================

About
-----

Why does this exist? RBDL supports urdf but not sdf. This is a basic and untested converter, that hopefully helps convert your sdf to a urdf. It will only copy over the kinematic, mass, and visual graphic tags. This does not support the full sdf or urdf specifications, see below for more details.

Why not just use URDF? SDF is a more feature rich format. For example, it and allows specifying closed kinematic chains. Additionally, we need to specify our Gazebo plugins in the SDF. Therefore, it makes more sense to maintain the SDF, and generate to the URDF. However, if you want to generate sdf from urdf, Gazebo has a converter ($ gz sdf -p MODEL.urdf) or ($ gz sdf -p MODEL.urdf > MODEL.sdf).

However, sdf does have some nuances. Joint pose is specified relative the child link's origin. This means that we must specify joint poses indirectly, through the link pose.

Here is an example. Imagine we have a three link robot arm. Each link is 0.5 meter long, with a joint at either end. All link's have their origin at one end. The kinematic structure is:

World -> joint0 -> link0 -> joint1 -> link1 -> joint2 -> link2

In a urdf, we would say:

* Every joint's origin is (0, 0, 0.5), in the frame of their parent link.
* Every link's  origin is (0, 0, 0), in the frame of their parent joint. 

In sdf, we would say:

* Every joint's origin is (0, 0, 0), in the frame of their child link.
* Link0's origin is (0, 0, 0.0), in the world frame
* Link1's origin is (0, 0, 0.5), in the world frame
* Link2's origin is (0, 0, 1.0), in the world frame 

This converter is only handles the case where sdf uses no frame attributes for a pose, all joint poses are (0,0,0,0,0,0).

Run
---

      sdf2urdf ../kuka_iiwa/kuka_iiwa.urdf


[rviz user guide]:http://wiki.ros.org/rviz/UserGuide



