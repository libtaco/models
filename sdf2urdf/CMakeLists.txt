cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(sdf2urdf)

# CMAKE OPTIONS
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_BUILD_TYPE Debug)
SET(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

# BUILD SOURCES AND LIBRARY
set(SOURCES main.cpp
)

# Create a library
add_executable(${PROJECT_NAME} ${SOURCES})

# Link TinyXML
find_path( TINYXML_INCLUDE_DIRS "tinyxml.h" PATH_SUFFIXES "tinyxml" )
find_library( TINYXML_LIBRARY_DIRS NAMES "tinyxml" PATH_SUFFIXES "tinyxml" )
include_directories(${TINYXML_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${TINYXML_LIBRARY_DIRS})

# EIGEN LIBARARY
find_package (Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})
